class PotionsController < ApplicationController
  before_action :find_potion, only: [:show, :edit, :update, :destroy]

  def index
    @potion = Potion.all.order("created_at DESC")
  end

  def new
    @potion = Potion.new
  end

  def show
  end

  def create
    @potion = Potion.new(potion_params)
    if @potion.save
      redirect_to @potion, notice: "Successfully created a new potion."
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @potion.update(potion_params)
      redirect_to @potion, notice: "Successfully updated your new potion."
    else
      render 'edit'
    end
  end

  def destroy
    @potion.destroy
    redirect_to root_path, notice: "Successfully deleted your potion."
  end

  private

    def potion_params
      params.require(:potion).permit(:title, :description, :image)
    end

    def find_potion
      @potion = Potion.find(params[:id])
    end

end
