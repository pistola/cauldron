class AddAttachmentImageToPotions < ActiveRecord::Migration
  def self.up
    change_table :potions do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :potions, :image
  end
end
